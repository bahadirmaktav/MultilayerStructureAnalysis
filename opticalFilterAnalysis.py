from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from numpy import double, dtype
from sources.structureAnalysis import StructureAnalysis, np


class MainLayout(BoxLayout):
    pass


class MaterialProperitiesInputLayout(BoxLayout):
    pass


class DesignWavelengthInputLayout(BoxLayout):
    pass


class ThicknessDividerCoefficientLayout(BoxLayout):
    pass


class WavelengthRangeLayout(BoxLayout):
    pass


class StepSizeIn1nmLayout(BoxLayout):
    pass


class MultilayerSequenceLayout(BoxLayout):
    pass


class LabelWidget(Label):
    pass


class TextInputWidget(TextInput):
    pass


class OpticalFilterAnalysis(App):
    def autoThicknessControl(self, refractiveIndexStr, pressedToogleWidget):
        designWavelength = float(self.root.ids.designWavelengthLay.ids.designWavelength.text)
        thicknessDividerCoefficient = float(self.root.ids.thicknessDividerCoefficientLay.ids.thicknessDividerCoefficient.text)
        refractiveIndex = complex(refractiveIndexStr).real # TODO Will be change after complex refractive index handling implementation.
        pressedToogleWidget.text = str(designWavelength/(refractiveIndex*thicknessDividerCoefficient))
    def stateToBoolString(self, state):
        return True if state == "down" else False
    def analyze(self):
        thicknessArr = np.ones((8), dtype=double)
        thicknessArr[0] = float(self.root.ids.materials.ids.layerThi1.text)
        thicknessArr[1] = float(self.root.ids.materials.ids.layerThi2.text)
        thicknessArr[2] = float(self.root.ids.materials.ids.layerThi3.text)
        thicknessArr[3] = float(self.root.ids.materials.ids.layerThi4.text)
        thicknessArr[4] = float(self.root.ids.materials.ids.layerThi5.text)
        thicknessArr[5] = float(self.root.ids.materials.ids.layerThi6.text)
        thicknessArr[6] = 50 # Air thickness is not needed.
        thicknessArr[7] = 50 # Substrate thickness is not needed.
        refractiveIndexArr = np.ones((8), dtype=complex)
        refractiveIndexArr[0] = complex(self.root.ids.materials.ids.layerRef1.text)
        refractiveIndexArr[1] = complex(self.root.ids.materials.ids.layerRef2.text)
        refractiveIndexArr[2] = complex(self.root.ids.materials.ids.layerRef3.text)
        refractiveIndexArr[3] = complex(self.root.ids.materials.ids.layerRef4.text)
        refractiveIndexArr[4] = complex(self.root.ids.materials.ids.layerRef5.text)
        refractiveIndexArr[5] = complex(self.root.ids.materials.ids.layerRef6.text)
        refractiveIndexArr[6] = complex(self.root.ids.materials.ids.layerRef7.text)
        refractiveIndexArr[7] = complex(self.root.ids.materials.ids.layerRef8.text)
        isThicknessAutoCalculatedArr = np.ones((8), dtype=bool)
        isThicknessAutoCalculatedArr[0] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto1.state)
        isThicknessAutoCalculatedArr[1] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto2.state)
        isThicknessAutoCalculatedArr[2] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto3.state)
        isThicknessAutoCalculatedArr[3] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto4.state)
        isThicknessAutoCalculatedArr[4] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto5.state)
        isThicknessAutoCalculatedArr[5] = self.stateToBoolString(self.root.ids.materials.ids.layerAuto6.state)
        isThicknessAutoCalculatedArr[6] = False # Air thickness auto bool is not needed.
        isThicknessAutoCalculatedArr[7] = False # Substrate thickness auto bool is not needed.
        thicknessDividerCoefficient = float(self.root.ids.thicknessDividerCoefficientLay.ids.thicknessDividerCoefficient.text)
        designWaveLength = float(self.root.ids.designWavelengthLay.ids.designWavelength.text)
        wavelengthRangeMin = float(self.root.ids.wavelengthRangeLay.ids.wavelengthRangeMin.text)
        wavelengthRangeMax = float(self.root.ids.wavelengthRangeLay.ids.wavelengthRangeMax.text)
        stepSize = float(self.root.ids.stepSizeLay.ids.stepSize.text)
        wavelengthRange = np.arange(wavelengthRangeMin, wavelengthRangeMax, stepSize)
        multilayerSequence = self.root.ids.multilayerSequenceLay.ids.multilayerSequenceBegin.text + self.root.ids.multilayerSequenceLay.ids.multilayerSequenceMid.text + self.root.ids.multilayerSequenceLay.ids.multilayerSequenceLast.text
        thermalExpCoeffArr = np.array([0.0000005,0.0000026,0.00000063,0,0,0,0,0]) #TODO Initialization for test purposes.
        thermoOpticCoeffArr = np.array([0.000186,0.0000068,0.0000035,0,0,0,0,0])  #TODO Initialization for test purposes.
        temperature = 20                                                          #TODO Initialization for test purposes.
        StructureAnalysis(designWaveLength=designWaveLength, isThicknessAutoCalculatedArr=isThicknessAutoCalculatedArr, multilayerSequence=multilayerSequence, 
                    refractiveIndexArr=refractiveIndexArr, thicknessArr=thicknessArr, thicknessDividerCoefficient=thicknessDividerCoefficient, 
                    thermalExpCoeffArr=thermalExpCoeffArr, thermoOpticCoeffArr=thermoOpticCoeffArr, temperature=temperature).plotTheResults(wavelengthRange=wavelengthRange)
    def build(self):
        return MainLayout()


if __name__ == '__main__':
    Window.clearcolor = (0.396, 0.298, 0.309, 1)
    OpticalFilterAnalysis().run()
