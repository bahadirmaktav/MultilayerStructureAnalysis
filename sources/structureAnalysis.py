import matplotlib.pyplot as plt
import sources.constants as cs
from sources.multilayerAnalysis import MultilayerAnalysis, np

class StructureAnalysis(MultilayerAnalysis):
    def __init__(self, designWaveLength, thicknessDividerCoefficient, refractiveIndexArr, isThicknessAutoCalculatedArr, thicknessArr, thermalExpCoeffArr, thermoOpticCoeffArr, temperature, multilayerSequence, wavelength=1):
        self.designWaveLength = designWaveLength
        self.thicknessDividerCoefficient = thicknessDividerCoefficient
        self.wavelength = wavelength
        self.refractiveIndexArr = refractiveIndexArr
        self.isThicknessAutoCalculatedArr = isThicknessAutoCalculatedArr
        self.thicknessArr = thicknessArr
        self.matrixTypeArr = cs.LAYER_MATRIX_TYPE_ARR
        self.thermalExpCoeffArr = thermalExpCoeffArr
        self.thermoOpticCoeffArr = thermoOpticCoeffArr
        self.temperature = temperature
        self.layerMatrixTypesArr = np.ones((cs.LAYER_TYPE_NUM, 2, 2), dtype=complex)
        self.multilayerSequence = multilayerSequence.upper()
        self.firstLayerRefractiveIndex = refractiveIndexArr[cs.LAYER_TYPE_NUM - 2]
        self.lastLayerRefractiveIndex = refractiveIndexArr[cs.LAYER_TYPE_NUM - 1]
        self.setThicknessArrForAuto()

    def initializeLayerMatrixTypes(self, wavelength):
        self.wavelength = wavelength
        for k in range(cs.LAYER_TYPE_NUM):
            if(self.indexToChar(k) in self.multilayerSequence):
                self.layerMatrixTypesArr[k] = super().layerMatrixReturnCal(matrixType=self.matrixTypeArr[k], refractiveIndex=self.refractiveIndexArr[k], thickness=self.thicknessArr[k], wavelength=self.wavelength)
            else:
                self.layerMatrixTypesArr[k] = np.ones((2,2),dtype=complex)

    def setThicknessArrForAuto(self):
        for k in range(cs.LAYER_TYPE_NUM):
            if(self.indexToChar(k) in self.multilayerSequence):
                self.thicknessArr[k]=(self.designWaveLength / (self.thicknessDividerCoefficient * self.refractiveIndexArr[k]).real) if self.isThicknessAutoCalculatedArr[k] else self.thicknessArr[k]

    def totalSystemMatrixCal(self, multilayerSequence):
        totalSystemMatrix = np.identity(2,dtype=complex)
        index = 0
        openParentheses = 0
        while index < len(multilayerSequence):
            if(multilayerSequence[index] == '['):
                openParentheses += 1
                index += 1
                newMultilayerSequence = ""
                while(openParentheses != 1 or multilayerSequence[index] != ']'):
                    if(multilayerSequence[index] == '['):
                        openParentheses += 1
                    elif(multilayerSequence[index] == ']'):
                        openParentheses -= 1
                    newMultilayerSequence += multilayerSequence[index]
                    index += 1
                betweenBracketsMatrixHolder = self.totalSystemMatrixCal(newMultilayerSequence)
                openParentheses -= 1
                index += 1
                powerVal = 1
                if(multilayerSequence[index] == '^'):
                    index += 1
                    if(multilayerSequence[index] == '('):
                        index += 1
                        powerString = multilayerSequence[index]
                        index += 1
                        while multilayerSequence[index] != ')':
                            powerString += multilayerSequence[index]
                            index += 1
                        powerVal = int(powerString)
                        index += 1
                bracketsMatrixPowered = betweenBracketsMatrixHolder
                for i in range(0, powerVal-1):
                    bracketsMatrixPowered = bracketsMatrixPowered.dot(betweenBracketsMatrixHolder)
                totalSystemMatrix = totalSystemMatrix.dot(bracketsMatrixPowered)
            else:
                totalSystemMatrix = totalSystemMatrix.dot(self.charToLayerMatrix(multilayerSequence[index]))
                index += 1
        return totalSystemMatrix
    
    def charToLayerMatrix(self, char):
        if(char == 'H'):
            return self.layerMatrixTypesArr[0]
        elif(char == 'L'):
            return self.layerMatrixTypesArr[1]
        elif(char == 'M'):
            return self.layerMatrixTypesArr[2]
        elif(char == 'D'):
            return self.layerMatrixTypesArr[3]
        elif(char == 'T'):
            return self.layerMatrixTypesArr[4]
        elif(char == 'V'):
            return self.layerMatrixTypesArr[5]
        elif(char == 'A'):
            return self.layerMatrixTypesArr[6]
        elif(char == 'S'):
            return self.layerMatrixTypesArr[7]
        else:
            return np.identity(2,dtype=complex)

    def indexToChar(self, k):
        if(k == 0):
            return 'H'
        elif(k == 1):
            return 'L'
        elif(k == 2):
            return 'M'
        elif(k == 3):
            return 'D'
        elif(k == 4):
            return 'T'
        elif(k == 5):
            return 'V'
        elif(k == 6):
            return 'A'
        elif(k == 7):
            return 'S'
        else:
            return ' '

    def plotTheResults(self, wavelengthRange):
        absorptance = []
        reflectance = []
        transmittance = []
        instance = StructureAnalysis(designWaveLength=self.designWaveLength,
                                     thicknessDividerCoefficient=self.thicknessDividerCoefficient,
                                     refractiveIndexArr=self.refractiveIndexArr,
                                     isThicknessAutoCalculatedArr=self.isThicknessAutoCalculatedArr,
                                     thicknessArr=self.thicknessArr,
                                     thermalExpCoeffArr=self.thermalExpCoeffArr,
                                     thermoOpticCoeffArr=self.thermoOpticCoeffArr,
                                     temperature= self.temperature,
                                     multilayerSequence=self.multilayerSequence)
        for wavelengthCounter in wavelengthRange:
            instance.initializeLayerMatrixTypes(wavelength=wavelengthCounter)
            totalSystemMatrix = instance.totalSystemMatrixCal(instance.multilayerSequence)
            #print(totalSystemMatrix)
            #print()
            reflectance.append(instance.reflectanceCal(totalSystemMatrix=totalSystemMatrix))
            #print(instance.wavelength)
            transmittance.append(instance.transmittanceCal(totalSystemMatrix=totalSystemMatrix, firstLayerRefractiveIndex=self.firstLayerRefractiveIndex, lastLayerRefractiveIndex=self.lastLayerRefractiveIndex))
            absorptance.append(instance.absorptanceCal(totalSystemMatrix=totalSystemMatrix, firstLayerRefractiveIndex=self.firstLayerRefractiveIndex, lastLayerRefractiveIndex=self.lastLayerRefractiveIndex))
        # fig, axs = plt.subplots(3, figsize=(10, 8))
        fig, axs = plt.subplots(2, figsize=(14, 8))
        axs[0].plot(wavelengthRange, reflectance, 'tab:blue')
        axs[1].plot(wavelengthRange, transmittance, 'tab:green')
        # axs[2].plot(wavelengthRange, absorptance, 'tab:orange')
        axs[0].set(ylabel='Yansıyan Güç Oranı',xlabel='Dalga Boyu (nm)')
        axs[1].set(ylabel='Transmittance',xlabel='Wavelength(nm)')
        # axs[2].set(ylabel='Absorptance', xlabel='Wavelength(nm)')  
        axs[0].grid(True)
        axs[1].grid(True)
        # axs[2].grid(True)
        fig.show()
        